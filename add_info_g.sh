echo "<pre>" >> dist/index.html

echo "<a href="https":"//gitlab.com/elvislittle/bmi/-/commits/master"><img alt="pipeline status" src="https":"//gitlab.com/elvislittle/bmi/badges/master/pipeline.svg" /></a>" >> dist/index.html

echo "<br>" >> dist/index.html

echo "creation date":"        $(date)" >> dist/index.html

echo "<br>" >> dist/index.html

echo "<b>build stage</b>" >> dist/index.html
echo "ci tool":"              gitlab ci" >> dist/index.html
echo "runner":"               ec2 ('jenkins-slave')" >> dist/index.html
echo "executor":"             docker" >> dist/index.html
echo "stage steps type":"     running shell scripts in docker" >> dist/index.html

echo "<b>deploy stage</b>" >> dist/index.html
echo "ci tool":"              gitlab ci" >> dist/index.html
echo "runner":"               ec2 ('jenkins-slave')" >> dist/index.html
echo "executor":"             shell" >> dist/index.html
echo "stage steps type":"     running shell scripts" >> dist/index.html
echo "deploy to":"            ec2 (nginx-sandbox)" >> dist/index.html
echo "deploy location":"      bmi-g" >> dist/index.html

echo "<b>elvislittle.site</b>" >> dist/index.html
echo "points to 'jenkins-master' ec2 running jenkins" >> dist/index.html
echo "uses route 53 dns service" >> dist/index.html
echo "uses non-caching cloudfront distribution with amazon ssl certificate" >> dist/index.html
echo "serves jenkins via nginx with let's encrypt ssl certificate" >> dist/index.html

echo "<b>elvislittle.online</b>" >> dist/index.html
echo "points to 'nginx-sandbox' ec2 running nginx" >> dist/index.html
echo "uses route 53 dns service" >> dist/index.html
echo "uses non-caching cloudfront distribution with amazon ssl certificate""       " >> dist/index.html
echo "serves bmi via nginx with let's encrypt ssl certificate" >> dist/index.html

echo "</pre>" >> dist/index.html