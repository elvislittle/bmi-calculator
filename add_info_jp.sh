echo "<pre>" >> dist/index.html

echo "<a href='https://www.elvislittle.site/job/bmi/job/bmi-jp/badge/'><img src='http://www.elvislittle.site/buildStatus/icon?job=bmi%2Fbmi-jp'></a>" >> dist/index.html

echo "<br>" >> dist/index.html

echo "creation date:\t\t$(date)" >> dist/index.html

echo "<br>" >> dist/index.html

echo "<b>build stage</b>" >> dist/index.html
echo "ci tool:\t\tjenkins (pipeline job)" >> dist/index.html
echo "jenkins controller:\tec2 ('jenkins-master')" >> dist/index.html
echo "jenkins agent:\t\tec2 ('jenkins-slave')" >> dist/index.html
echo "jenkins agent type:\tdocker" >> dist/index.html
echo "stage steps type:\trunning shell scripts in docker" >> dist/index.html

echo "<b>deploy stage</b>" >> dist/index.html
echo "ci tool:\t\tjenkins (pipeline job)" >> dist/index.html
echo "jenkins controller:\tec2 ('jenkins-master')" >> dist/index.html
echo "jenkins agent:\t\tec2 ('jenkins-slave')" >> dist/index.html
echo "jenkins agent type:\tssh" >> dist/index.html
echo "stage steps type:\trunning shell scripts" >> dist/index.html
echo "deploy to:\t\tec2 ('nginx-sandbox')" >> dist/index.html
echo "deploy location:\tbmi-jp" >> dist/index.html

echo "<b>elvislittle.site</b>" >> dist/index.html
echo "points to 'jenkins-master' ec2 running jenkins" >> dist/index.html
echo "uses route 53 dns service" >> dist/index.html
echo "uses non-caching cloudfront distribution with amazon ssl certificate" >> dist/index.html
echo "serves jenkins via nginx with let's encrypt ssl certificate" >> dist/index.html

echo "<b>elvislittle.online</b>" >> dist/index.html
echo "points to 'nginx-sandbox' ec2 running nginx" >> dist/index.html
echo "uses route 53 dns service" >> dist/index.html
echo "uses non-caching cloudfront distribution with amazon ssl certificate""       " >> dist/index.html
echo "serves bmi via nginx with let's encrypt ssl certificate" >> dist/index.html

echo "</pre>" >> dist/index.html
